This patches WickedGL 3.02 Beta to remove the annoying "Press C to Continue" message at startup.

Put the utility in the same directory as the opengl32.dll and run the patch.
