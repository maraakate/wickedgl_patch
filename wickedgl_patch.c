/*
Copyright (C) 2019 by Frank Sapone

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

long offset1_v5 = 0x0002975B;
long offset2_v5 = 0x00029788;
long offset3_v5 = 0x000298CB;
long offset4_v5 = 0x000298D4;
long offset5_v5 = 0x0004F355;

long offset1 = 0x0002560B;
long offset2 = 0x00025638;
long offset3 = 0x0002577B;
long offset4 = 0x00025784;
long offset5 = 0x0004A355;

unsigned char noop[] = { 144 };
unsigned char offset1_patch[] = { 184, 0, 0, 0, 0, 144, 144, 144 };
unsigned char offset5_patch[] = { 81 };

FILE *f = NULL;

extern unsigned short CRC_Block (unsigned char *start, int count);

void Patch_Voodoo5 (void)
{
	int i = 0;

	fseek(f, offset1_v5, SEEK_SET);
	fwrite((unsigned char *)offset1_patch, sizeof(offset1_patch), 1, f);

	fseek(f, offset2_v5, SEEK_SET);
	for (i = 0; i < 37; i++)
	{
		fwrite((unsigned char *)noop, sizeof(noop), 1, f);
	}

	fseek(f, offset3_v5, SEEK_SET);
	for (i = 0; i < 5; i++)
	{
		fwrite((unsigned char *)noop, sizeof(noop), 1, f);
	}

	fseek(f, offset4_v5, SEEK_SET);
	for (i = 0; i < 5; i++)
	{
		fwrite((unsigned char *)noop, sizeof(noop), 1, f);
	}

	fseek(f, offset5_v5, SEEK_SET);
	fwrite((unsigned char *)offset5_patch, sizeof(offset5_patch), 1, f);
}

void Patch_Normal (void)
{
	int i = 0;

	fseek(f, offset1, SEEK_SET);
	fwrite((unsigned char *)offset1_patch, sizeof(offset1_patch), 1, f);

	fseek(f, offset2, SEEK_SET);
	for (i = 0; i < 37; i++)
	{
		fwrite((unsigned char *)noop, sizeof(noop), 1, f);
	}

	fseek(f, offset3, SEEK_SET);
	for (i = 0; i < 5; i++)
	{
		fwrite((unsigned char *)noop, sizeof(noop), 1, f);
	}

	fseek(f, offset4, SEEK_SET);
	for (i = 0; i < 5; i++)
	{
		fwrite((unsigned char *)noop, sizeof(noop), 1, f);
	}

	fseek(f, offset5, SEEK_SET);
	fwrite((unsigned char *)offset5_patch, sizeof(offset5_patch), 1, f);
}

int main(int argc, char* argv[])
{
	unsigned char *buff = NULL;
	unsigned short crc = 0;
	long len = 0;

	f = fopen("opengl32.dll", "r+b");
	if (!f)
	{
		printf("Can't open opengl32.dll!  Press any key to exit...\n");
		getch();
		return -1;
	}

	fseek(f, 0, SEEK_END);
	len = ftell(f);
	if (len <= 0)
	{
		printf("Error reading opengl32.dll!  Press any key to exit...\n");
		getch();
		return -1;
	}

	buff = calloc(len, sizeof(unsigned char));
	if (!buff)
	{
		printf("Error allocating temporary buffer!  Press any key to exit...\n");
		getch();
		return -1;
	}

	fseek(f, 0, SEEK_SET);
	fread(buff, len * sizeof(unsigned char), 1, f);
	crc = CRC_Block(buff, len);
	free(buff);

	if (crc == 24956)
	{
		printf("Patching WickedGL v3.02 Voodoo 5 version...\n");
		Patch_Voodoo5();
	}
	else if (crc == 17044)
	{
		printf("Patching WickedGL v3.02 Regular version...\n");
		Patch_Normal();
	}
	else
	{
		printf("Error invalid CRC for opengl32.dll.  Wrong version?  Press any key to exit...\n");
		getch();
		fclose(f);
		return -1;
	}

	fflush(f);
	fclose(f);
	printf("Done!  Press any key to exit...\n");
	getch();
	return 0;
}
